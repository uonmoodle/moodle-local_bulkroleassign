@local @uon @local_bulkroleassign
Feature: Rule deletion
    In order to remove assignments
    As an admin
    I need to be able to delete a rule.

    Background:
        Given the following "categories" exist:
            | name | category | idnumber |
            | Cat 1 | 0 | CAT1 |
        And the following "users" exist:
            | username | firstname | lastname | email | city |
            | student1 | Sam | Student | student1@example.com | Nottingham |
            | teacher1 | Teacher | One | teacher1@example.com | London |
            | teacher2 | Tutor | Two | teacher2@example.com | Northampton |
            | teacher3 | Lecturer | Three | teacher3@example.com | Nottingham |
        And I create a user info field:
            | shortname | name | datatype | defaultdata |
            | tst | test | text | 0 |
        And I create user info data:
            | username | fieldshortname | data |
            | student1 | tst | 1 |
            | teacher1 | tst | 2 |
            | teacher2 | tst | 1 |
            | teacher3 | tst | 2 |

    Scenario: Delete a rule
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
        And I am on the "local_bulkroleassign > Administration" page logged in as "admin"
        And I should see "Test rule"
        But I should not see "There are no rules currently set up"
        When I press "Delete"
        And I press "Continue"
        Then I should see "There are no rules currently set up"
        But I should not see "Test rule"
