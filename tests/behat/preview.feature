@local @uon @local_bulkroleassign
Feature: Rule preview
    In order to verify a rule
    As an admin
    I need to be see which users a rule will select.

    Background:
        Given the following "categories" exist:
            | name | category | idnumber |
            | Cat 1 | 0 | CAT1 |
        And the following "users" exist:
            | username | firstname | lastname | email | city |
            | student1 | Sam | Student | student1@example.com | Nottingham |
            | teacher1 | Teacher | One | teacher1@example.com | London |
            | teacher2 | Tutor | Two | teacher2@example.com | Northampton |
            | teacher3 | Lecturer | Three | teacher3@example.com | Nottingham |
        And I create a user info field:
            | shortname | name | datatype | defaultdata |
            | tst | test | text | 0 |
        And I create user info data:
            | username | fieldshortname | data |
            | student1 | tst | 1 |
            | teacher1 | tst | 2 |
            | teacher2 | tst | 1 |
            | teacher3 | tst | 2 |

    Scenario: Preview core profile rule
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
        When I am on the "local_bulkroleassign > Administration" page logged in as "admin"
        And I press "Preview"
        Then I should see "Sam Student"
        And I should see "student1@example.com"
        And I should see "student1"
        And I should see "Lecturer Three"
        And I should see "teacher3@example.com"
        And I should see "teacher3"
        But I should not see "Teacher One"
        And I should not see "Tutor Two"

    Scenario: Preview custom profile rule
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | custom | tst | equals | 2 |
        When I am on the "local_bulkroleassign > Administration" page logged in as "admin"
        And I press "Preview"
        Then I should see "Teacher One"
        And I should see "teacher1@example.com"
        And I should see "teacher1"
        And I should see "Lecturer Three"
        And I should see "teacher3@example.com"
        And I should see "teacher3"
        But I should not see "Sam Student"
        And I should not see "Tutor Two"

    Scenario: Preview core and custom profile rule
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
            | custom | tst | equals | 2 |
        When I am on the "local_bulkroleassign > Administration" page logged in as "admin"
        And I press "Preview"
        Then I should see "Lecturer Three"
        And I should see "teacher3@example.com"
        And I should see "teacher3"
        But I should not see "Sam Student"
        And I should not see "Teacher One"
        And I should not see "Tutor Two"
