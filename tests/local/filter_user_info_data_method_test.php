<?php
// This file is part of the bulkroleassgin plugin in Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

use context_coursecat;

/**
 * Test that the filter methods work correctly for
 * the \local_bulkroleassign\local\filter_user class.
 *
 * this is essentially testing the SQL generation of rules.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group local_bulkroleassign
 * @group uon
 */
class filter_user_info_data_method_test extends \advanced_testcase {
    /** @var stdClass Stores a rule that should match user 1 and 2. */
    protected $rule1;
    /** @var stdClass Stores a user that should match rule 1. */
    protected $user1;
    /** @var stdClass Stores a user that should match rule 1. */
    protected $user2;
    /** @var stdClass Stores a user that should not match rule 1. */
    protected $user3;
    /** @var stdClass Stores the custom user profile field for use in the test. */
    protected $profilefield;

    /**
     * @see \TestCase::setUp
     */
    public function setUp(): void {
        global $DB;
        parent::setUp();
        $this->resetAfterTest(true);
        $category = self::getDataGenerator()->create_category();
        $this->user1 = self::getDataGenerator()->create_user();
        $this->user2 = self::getDataGenerator()->create_user();
        $this->user3 = self::getDataGenerator()->create_user();
        $context = context_coursecat::instance($category->id);
        $role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1params = array(
            'roleid' => $role,
            'contextid' => $context->id,
        );
        $this->rule1 = $generator->create_rule($rule1params);
        // Create a custom user profile field.
        $this->profilefield = $generator->create_userinfofield();
        // Set the vaules in it for the users we created.
        $generator->create_userinfodata(
            array('fieldid' => $this->profilefield->id, 'userid' => $this->user1->id, 'data' => 'nottingham')
        );
        $generator->create_userinfodata(
            array('fieldid' => $this->profilefield->id, 'userid' => $this->user2->id, 'data' => 'london')
        );
        $generator->create_userinfodata(
            array('fieldid' => $this->profilefield->id, 'userid' => $this->user3->id, 'data' => 'northampton')
        );
    }

    /**
     * @see \TestCase::tearDown
     */
    public function tearDown(): void {
        rule::reset();
        $this->rule1 = null;
        $this->user1 = null;
        $this->user2 = null;
        $this->user3 = null;
        parent::tearDown();
    }

    /**
     * Test that the equals filter method works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @covers \local_bulkroleassign\local\filter_user_info_data::sql_equals
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_equals() {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $filter1params = array(
            'ruleid' => $this->rule1->id,
            'fldtype' => filter::TYPE_CUSTOM,
            'userfld' => $this->profilefield->id,
            'method' => filter::METHOD_EQUALS,
            'filter_value' => 'nottingham',
        );
        $this->filter1 = $generator->create_filter($filter1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // There should be one role assignments for the rule.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        $this->assertEquals(1, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Test that the begins filter method works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @covers \local_bulkroleassign\local\filter_user_info_data::sql_begins
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_begins_match() {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $filter1params = array(
            'ruleid' => $this->rule1->id,
            'fldtype' => filter::TYPE_CUSTOM,
            'userfld' => $this->profilefield->id,
            'method' => filter::METHOD_BEGINS,
            'filter_value' => 'n',
        );
        $this->filter1 = $generator->create_filter($filter1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // There should be one role assignments for the rule.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 3 was assigned.
        $params['userid'] = $this->user3->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Test that the ends filter method works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @covers \local_bulkroleassign\local\filter_user_info_data::sql_ends
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_ends_match() {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $filter1params = array(
            'ruleid' => $this->rule1->id,
            'fldtype' => filter::TYPE_CUSTOM,
            'userfld' => $this->profilefield->id,
            'method' => filter::METHOD_ENDS,
            'filter_value' => 'n',
        );
        $this->filter1 = $generator->create_filter($filter1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // There should be one role assignments for the rule.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 2 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 3 was assigned.
        $params['userid'] = $this->user3->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Test that the contains filter method works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @covers \local_bulkroleassign\local\filter_user_info_data::sql_contains
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_contains_match() {
        global $DB;
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $filter1params = array(
            'ruleid' => $this->rule1->id,
            'fldtype' => filter::TYPE_CUSTOM,
            'userfld' => $this->profilefield->id,
            'method' => filter::METHOD_CONTAINS,
            'filter_value' => 'ham',
        );
        $this->filter1 = $generator->create_filter($filter1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // There should be one role assignments for the rule.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 3 was assigned.
        $params['userid'] = $this->user3->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }
}
