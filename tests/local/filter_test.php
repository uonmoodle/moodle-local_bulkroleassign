<?php
// This file is part of the bulkroleassgin plugin in Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

/**
 * Test the static methods of the \local_bulkroleassign\local\filter abstract class.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group local_bulkroleassign
 * @group uon
 */
class filter_test extends \advanced_testcase {
    /**
     * Tests that the filter object will load the correct filter data from the database.
     *
     * @covers \local_bulkroleassign\local\filter::get_filters
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_load() {
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        // Setup.
        $rule1 = $generator->create_rule();
        $rule2 = $generator->create_rule();
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $filter2 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CUSTOM));
        // Create a filter for another rule to ensure that it is not loaded.
        $generator->create_filter(array('ruleid' => $rule2->id));
        // The test.
        $rule = new rule($rule1->id);
        $filters = filter::get_filters($rule);
        // Check that the correct filters have been loaded.
        $this->assertCount(2, $filters);
        $this->assertArrayHasKey($filter1->id, $filters);
        $this->assertArrayHasKey($filter2->id, $filters);
        // Check that filter 1 loaded correctly.
        $this->assertInstanceOf('\local_bulkroleassign\local\filter_user', $filters[$filter1->id]);
        $this->assertEquals($filter1->id, $filters[$filter1->id]->id);
        $this->assertEquals($filter1->method, $filters[$filter1->id]->method);
        $this->assertInstanceOf('\local_bulkroleassign\local\rule', $filters[$filter1->id]->rule);
        $this->assertEquals($filter1->ruleid, $filters[$filter1->id]->rule->id);
        $this->assertEquals($filter1->filter_value, $filters[$filter1->id]->value);
        $this->assertEquals($filter1->userfld, $filters[$filter1->id]->field);
        $this->assertEquals($filter1->fldtype, $filters[$filter1->id]->type);
        // Check that filter 2 has loaded correctly.
        $this->assertInstanceOf('\local_bulkroleassign\local\filter_user_info_data', $filters[$filter2->id]);
        $this->assertEquals($filter2->id, $filters[$filter2->id]->id);
        $this->assertEquals($filter2->method, $filters[$filter2->id]->method);
        $this->assertInstanceOf('\local_bulkroleassign\local\rule', $filters[$filter2->id]->rule);
        $this->assertEquals($filter2->ruleid, $filters[$filter2->id]->rule->id);
        $this->assertEquals($filter2->filter_value, $filters[$filter2->id]->value);
        $this->assertEquals($filter2->userfld, $filters[$filter2->id]->field);
        $this->assertEquals($filter2->fldtype, $filters[$filter2->id]->type);
    }
}
