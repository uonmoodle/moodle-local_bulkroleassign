<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk Role Assignments - Upgrade script
 *
 * @package    local_bulkroleassign
 * @copyright  2012 and later Nottingham University
 * @author     Benjamin Ellis <benjamin.ellis@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Bulk Role Assignments - Upgrade script
 *
 * @package    local_bulkroleassign
 * @copyright  2012 and later Nottingham University
 * @author     Benjamin Ellis <benjamin.ellis@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @param int $oldversion
 * @return bool
 * @global moodle_database $DB
 */
function xmldb_local_bulkroleassign_upgrade($oldversion) {
    global $DB;
    $dbman = $DB->get_manager();
    $result = true;

    if ($oldversion < 2013081500) {
        // Rename bulkroleassign_userfilter to bulkroleassign_ufilter.
        // The table needs renaming as Moodle now throws exceptions when the name is this long.
        $table = new xmldb_table('bulkroleassign_userfilter');
        if ($dbman->table_exists($table)) {
            $dbman->rename_table($table, 'bulkroleassign_ufilter');
        }

        upgrade_plugin_savepoint(true, '2013081500', 'local', 'bulkroleassign');
        $result = $result && true;
    }

    if ($oldversion < 2014062510) {
        // Add foreign keys.
        $table = new xmldb_table('local_bulkroleassign_context');
        if ($dbman->table_exists($table)) {

            $index = new xmldb_index('ruleid', XMLDB_INDEX_NOTUNIQUE, array('ruleid'));

            if ($dbman->index_exists($table, $index)) {
                $dbman->drop_index($table, $index);
            }

            $key = new xmldb_key('ruleid', XMLDB_KEY_FOREIGN, array('ruleid'), 'local_bulkroleassign_rules', array('id'));

            $dbman->add_key($table, $key);
        }

        $table = new xmldb_table('local_bulkroleassign_ufilter');
        if ($dbman->table_exists($table)) {
            $index = new xmldb_index('rule_contextid', XMLDB_INDEX_NOTUNIQUE, array('rule_contextid'));

            if ($dbman->index_exists($table, $index)) {
                $dbman->drop_index($table, $index);
            }

            $key = new xmldb_key('rule_contextid', XMLDB_KEY_FOREIGN, array('rule_contextid'), 'local_bulkroleassign_context',
                    array('id'));

            $dbman->add_key($table, $key);
        }

        upgrade_plugin_savepoint(true, '2014062510', 'local', 'bulkroleassign');
        $result = $result && true;
    }

    if ($oldversion < 2014071016) {
        // Check for duplicates in existing data and rename.
        $sql = 'SELECT a.id, a.rule_name from {local_bulkroleassign_rules} a'
                . ' JOIN {local_bulkroleassign_rules} b'
                . ' ON a.rule_name = b.rule_name WHERE a.id <> b.id'
                . ' ORDER BY a.rule_name, a.id';

        $duplicates = $DB->get_records_sql($sql);

        if (!empty($duplicates)) {
            $previousname = '';
            $i = 1;

            foreach ($duplicates as $rule) {
                if ($previousname === $rule->rule_name) {
                    $i++;
                } else {
                    $i = 1;
                }

                $previousname = $rule->rule_name;
                $updatedrule = new stdClass();
                $updatedrule->id = $rule->id;
                $updatedrule->rule_name = "$rule->rule_name #$i";

                $DB->update_record('local_bulkroleassign_rules', $updatedrule);
            }
        }

        // Make index unique.
        $table = new xmldb_table('local_bulkroleassign_rules');
        if ($dbman->table_exists($table)) {
            $index = new xmldb_index('rulename', XMLDB_INDEX_NOTUNIQUE, array('rule_name'));

            if ($dbman->index_exists($table, $index)) {
                $dbman->drop_index($table, $index);
            }
            $newindex = new xmldb_index('rulename', XMLDB_INDEX_UNIQUE, array('rule_name'));
            $dbman->add_index($table, $newindex);
        }
        upgrade_plugin_savepoint(true, '2014071016', 'local', 'bulkroleassign');
        $result = $result && true;
    }

    if ($oldversion < 2017022800) {
        // This change simplifies the database structure by combining the
        // local_bulkroleassign_context table into the local_bulkroleassign_rules
        // table.
        //
        // It looks as though the original intention was to allow a rule to assign
        // users to multiple role/context combinations, however this has never been
        // implemented.
        //
        // Not having two tables will simplify queries and speed up writes.
        $rulestable = new xmldb_table('local_bulkroleassign_rules');
        // Only two fields are required from the table.
        $rolefeild = new xmldb_field('roleid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, 0);
        $roleindex = new xmldb_index('roleid', XMLDB_INDEX_NOTUNIQUE, array('roleid'));
        $contextfeild = new xmldb_field('contextid', XMLDB_TYPE_INTEGER, 10, null, XMLDB_NOTNULL, null, 0);
        $contextindex = new xmldb_index('contextid', XMLDB_INDEX_NOTUNIQUE, array('contextid'));
        if (!$dbman->field_exists($rulestable, $rolefeild)) {
            $dbman->add_field($rulestable, $rolefeild);
        }
        if (!$dbman->field_exists($rulestable, $contextfeild)) {
            $dbman->add_field($rulestable, $contextfeild);
        }
        if (!$dbman->index_exists($rulestable, $roleindex)) {
            $dbman->add_index($rulestable, $roleindex);
        }
        if (!$dbman->index_exists($rulestable, $contextindex)) {
            $dbman->add_index($rulestable, $contextindex);
        }

        $contexttable = new xmldb_table('local_bulkroleassign_context');
        // Now we need to transfer the data.
        $sql = "UPDATE {local_bulkroleassign_rules} r,  {local_bulkroleassign_context} c "
                . "SET r.roleid = c.roleid, r.contextid = c.contextid "
                . "WHERE r.id = c.ruleid";
        if ($dbman->table_exists($contexttable)) {
            $DB->execute($sql);
        }
        // We are also going to change the structure of the local_bulkroleassign_ufilter table,
        // so that the structure is standardised in a way that will allow for many types of
        // filter to exist side by side without growing the number of fields.
        //
        // The achive this the userfld_name and userinfo_fld_id fields will be replaced.
        // The function of both will be combined into a single field to hold the profile field that will
        // be searched. As the userfld_name field has never been used I will simply change the name
        // and type of the userinfo_fld_id to hold this.
        //
        // I will then repurpose the userfld_name field to hold the type of profile field that
        // will be searched by the filter.
        $filtertable = new xmldb_table('local_bulkroleassign_ufilter');
        $userfldname = new xmldb_field('userfld_name');
        $userinfofldid = new xmldb_field('userinfo_fld_id');
        $typefield = new xmldb_field(
            'fldtype',
            XMLDB_TYPE_CHAR,
            50,
            null,
            XMLDB_NOTNULL,
            null,
            \local_bulkroleassign\local\filter::TYPE_CUSTOM
        );
        $namefield = new xmldb_field('userfld', XMLDB_TYPE_CHAR, 50, null, XMLDB_NOTNULL, null, '0');
        $methodfield = new xmldb_field(
            'method',
            XMLDB_TYPE_INTEGER,
            10,
            null,
            XMLDB_NOTNULL,
            null,
            \local_bulkroleassign\local\filter::METHOD_EQUALS
        );
        $userfldnameindex = new xmldb_index('userfld_name', XMLDB_INDEX_NOTUNIQUE, array('userfld_name'));
        $userinfofldidindex = new xmldb_index('userinfo_fld_id', XMLDB_INDEX_NOTUNIQUE, array('userinfo_fld_id'));
        // Drop the indexes on the fields.
        if ($dbman->index_exists($filtertable, $userfldnameindex)) {
            $dbman->drop_index($filtertable, $userfldnameindex);
        }
        if ($dbman->index_exists($filtertable, $userinfofldidindex)) {
            $dbman->drop_index($filtertable, $userinfofldidindex);
        }
        // Add the new fields.
        if (!$dbman->field_exists($filtertable, $typefield)) {
            $dbman->add_field($filtertable, $typefield);
        }
        if (!$dbman->field_exists($filtertable, $namefield)) {
            $dbman->add_field($filtertable, $namefield);
        }
        if (!$dbman->field_exists($filtertable, $methodfield)) {
            $dbman->add_field($filtertable, $methodfield);
        }
        // Set the correct values on the fields.
        $sql = "UPDATE {local_bulkroleassign_ufilter} SET userfld = userinfo_fld_id, fldtype = :type";
        $params = array(
            'type' => \local_bulkroleassign\local\filter::TYPE_CUSTOM,
        );
        if ($dbman->field_exists($filtertable, $userinfofldid)) {
            $DB->execute($sql, $params);
        }
        // Delete the fields that are not needed.
        if ($dbman->field_exists($filtertable, $userfldname)) {
            $dbman->drop_field($filtertable, $userfldname);
        }
        if ($dbman->field_exists($filtertable, $userinfofldid)) {
            $dbman->drop_field($filtertable, $userinfofldid);
        }
        // We also need to change the filter table to link directly to the rules table.
        $contextkey = new xmldb_key('rule_contextid', XMLDB_KEY_FOREIGN);
        $dbman->drop_key($filtertable, $contextkey);
        $contextid = new xmldb_field('rule_contextid', XMLDB_TYPE_INTEGER, 10, XMLDB_NOTNULL);
        if ($dbman->field_exists($filtertable, $contextid)) {
            $dbman->rename_field($filtertable, $contextid, 'ruleid');
        }
        // Link the fields directly to the rules.
        $sql = "UPDATE {local_bulkroleassign_ufilter} f, {local_bulkroleassign_context} c "
                . "SET f.ruleid = c.ruleid WHERE c.id = f.ruleid";
        if ($dbman->table_exists($contexttable)) {
            $DB->execute($sql);
            $dbman->drop_table($contexttable);
        }
        // Index the rule ids in the filter table.
        $ruleindex = new xmldb_index('ruleid', XMLDB_INDEX_NOTUNIQUE, array('ruleid'));
        if (!$dbman->index_exists($filtertable, $ruleindex)) {
            $dbman->add_index($filtertable, $ruleindex);
        }
        // Delete all existing assignments as we cannot tell which rule they wete added by.
        // They will be readded via a scheduled task.
        $DB->delete_records('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => 0));
        upgrade_plugin_savepoint(true, '2017022800', 'local', 'bulkroleassign');
        $result = $result && true;
    }

    return $result;
}
