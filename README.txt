ABOUT
==========
The 'Bulk role assign' local plugin was developed on behalf of The University of Nottingham by
    Benjamin Ellis
    Joseph Baxter
    Neill Magill

This module may be distributed under the terms of the General Public License
(see http://www.gnu.org/licenses/gpl.txt for details)

PURPOSE
==========
The Bulk role assign local plugin is designed to allow administrators to create rules
based on custom user information fields to assign users to a role within a category.

The rules can be applied manually or via a scheduled cron job.

INSTALLATION
==========
The Bulk role assign local plugin follows the standard installation procedure.

1. Create folder <path to your moodle dir>/local/bulkroleassign.
2. Extract files from folder inside archive to created folder.
3. Visit page Home ► Site administration ► Notifications to complete installation.
