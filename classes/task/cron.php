<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Cron definition
 *
 * @package    local_bulkroleassign
 * @copyright  2014 and later Nottingham University
 * @author     Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
namespace local_bulkroleassign\task;

/**
 * timetable_calendar cron task class
 *
 * @package    local_bulkroleassign
 * @copyright  2014 and later Nottingham University
 * @author     Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @author     Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cron extends \core\task\scheduled_task {
    /**
     * Get a descriptive name for this task (shown to admins).
     *
     * @return string
     */
    public function get_name() {
        return get_string('crontask', 'local_bulkroleassign');
    }

    /**
     * Do the scheduled job.
     */
    public function execute() {
        $rules = \local_bulkroleassign\local\rule::get_all_rules();
        mtrace("Updating Role Assignments...");
        foreach ($rules as $rule) {
            try {
                mtrace("...running rule $rule->id", " ");
                $rule->run();
            } catch (\local_bulkroleassign\local\invalid_rule $e) {
                mtrace("...skipping invalid rule.");
            }
        }
        mtrace("All rules updated.");
    }
}
