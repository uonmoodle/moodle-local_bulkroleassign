<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

/**
 * Defines methods that an active filter must implement.
 *
 * This is for defining static methods that cannot be part of the filter abstract class.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
interface filter_active {
    /**
     * Get a list of valid types for this filter, in the form of an array where
     * the key is the type that can be stored and the value is the human readable name.
     *
     * @return array
     */
    public static function get_valid_types();

    /**
     * Resets any static caches on the filter. Should only be used by unit tests.
     *
     * @return void
     */
    public static function reset();
}
