<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->libdir/formslib.php");

/**
 * Form for creating a new rule.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class ruleform extends \moodleform {
    /**
     * The maximum number filters of the same type we will allow a user to create.
     */
    const MAXFILTERS = 5;

    /**
     * Defines the elements on the form.
     */
    public function definition() {
        // The main rule fields.
        $this->add_rule_elements();
        // The fields for core user profile filters.
        $this->add_core_filters();
        // The fields for custom user profile filters.
        $this->add_custom_profile_filters();
        // Add the main form action buttons.
        $this->add_standard_buttons();
    }

    /**
     * @see \moodleform::validation
     */
    public function validation($data, $files) {
        $errors = parent::validation($data, $files);
        if (!isset(rule::get_role_select()[$data['roleid']])) {
            // The role must be in the rolelist.
            $errors['roleid'] = get_string('invalidrole', 'local_bulkroleassign');
        }
        if (!isset(rule::get_category_select()[$data['categoryid']])) {
            // The category id is not valid.
            $errors['categoryid'] = get_string('invalidcategory', 'local_bulkroleassign');
        }
        // At least one filter must have been set.
        $filterset = false;
        for ($i = 1; $i <= self::MAXFILTERS; $i++) {
            $corefilterset = !empty($data["corefiltervalue$i"]);
            $customfilterset = !empty($data["customfiltervalue$i"]);
            if ($corefilterset || $customfilterset) {
                $filterset = true;
                break;
            }
        }
        if (!$filterset) {
            // We did not find a valid filter.
            $errors["corefiltergroup1"] = get_string('minfilters', 'local_bulkroleassign');
            $errors["customfiltergroup1"] = get_string('minfilters', 'local_bulkroleassign');
        }
        return $errors;
    }

    /**
     * The elements used for the rule record.
     */
    protected function add_rule_elements() {
        $mform = $this->_form;
        $titleattrs = ['maxlength' => 50];
        $mform->addElement('text', 'title', get_string('ruletitle', 'local_bulkroleassign'), $titleattrs);
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', get_string('required'), 'required');
        $mform->addHelpButton('title', 'ruletitle', 'local_bulkroleassign');

        $descriptionattrs = [
            'wrap' => 'virtual',
            'rows' => 3,
            'cols' => 60,
            'maxlength' => 150,
        ];
        $mform->addElement(
            'textarea',
            'description',
            get_string('ruledescription', 'local_bulkroleassign'),
            $descriptionattrs
        );
        $mform->setType('description', PARAM_TEXT);
        $mform->addRule('description', get_string('required'), 'required');
        $mform->addHelpButton('description', 'ruledescription', 'local_bulkroleassign');

        $mform->addElement('select', 'roleid', get_string('rulerole', 'local_bulkroleassign'),
                rule::get_role_select());
        $mform->setType('roleid', PARAM_INT);
        $mform->addRule('roleid', get_string('required'), 'required');
        $mform->addHelpButton('roleid', 'rulerole', 'local_bulkroleassign');

        $mform->addElement('autocomplete', 'categoryid', get_string('rulecategory', 'local_bulkroleassign'),
                rule::get_category_select());
        $mform->setType('categoryid', PARAM_INT);
        $mform->addRule('categoryid', get_string('required'), 'required');
        $mform->addHelpButton('categoryid', 'rulecategory', 'local_bulkroleassign');
    }

    /**
     * Form controls for core filters.
     */
    protected function add_core_filters() {
        $mform = $this->_form;
        $mform->addElement('header', 'corefiltersheader', get_string('corefilters', 'local_bulkroleassign'));
        for ($i = 1; $i <= self::MAXFILTERS; $i++) {
            $prefix = "corefilter";
            $elements = $this->get_core_profile_filter($prefix, $i);
            $mform->addGroup($elements, "{$prefix}group{$i}", $i, array(' '), false);
        }
    }

    /**
     * Get the elements to add a core profile filter.
     *
     * @param string $prefix The prefix for the filter.
     * @param int $number The number of the filter
     * @return array
     */
    protected function get_core_profile_filter($prefix, $number) {
        $mform = $this->_form;
        $elements = array();
        $elements[] = &$mform->createElement(
            'select',
            "{$prefix}field{$number}",
            get_string('selectcorefilters', 'local_bulkroleassign'),
            filter_user::get_valid_types()
        );
        $mform->setType("{$prefix}field{$number}", PARAM_ALPHANUM);
        $elements[] = &$mform->createElement('select', "{$prefix}method{$number}", '', filter::get_method_select());
        $mform->setType("{$prefix}method{$number}", PARAM_INT);
        $elements[] = &$mform->createElement('text', "{$prefix}value{$number}");
        $mform->setType("{$prefix}value{$number}", PARAM_TEXT);
        return $elements;
    }

    /**
     * Form controls for custom profile field filters.
     *
     * @return void
     */
    protected function add_custom_profile_filters() {
        if (empty(filter_user_info_data::get_valid_types())) {
            // No custom fields setup in Moodle.
            return;
        }
        $mform = $this->_form;
        $mform->addElement('header', 'customfiltersheader', get_string('customfilters', 'local_bulkroleassign'));
        // Add the profile filters.
        for ($i = 1; $i <= self::MAXFILTERS; $i++) {
            $prefix = "customfilter";
            $elements = $this->get_custom_profile_filter($prefix, $i);
            $mform->addGroup($elements, "{$prefix}group{$i}", $i, array(' '), false);
        }
    }

    /**
     * Gets the fields for a single custom user profile filter.
     *
     * @param string $prefix
     * @param int $number The number of the filter
     * @return array
     */
    protected function get_custom_profile_filter($prefix, $number) {
        $mform = $this->_form;
        $elements = array();
        $elements[] = &$mform->createElement(
            'select',
            "{$prefix}field{$number}",
            get_string('selectcustomfilters', 'local_bulkroleassign'),
            filter_user_info_data::get_valid_types()
        );
        $mform->setType("{$prefix}field{$number}", PARAM_ALPHANUM);
        $elements[] = &$mform->createElement('select', "{$prefix}method{$number}", '', filter::get_method_select());
        $mform->setType("{$prefix}method{$number}", PARAM_INT);
        $elements[] = &$mform->createElement('text', "{$prefix}value{$number}");
        $mform->setType("{$prefix}value{$number}", PARAM_TEXT);
        return $elements;
    }

    /**
     * Submit buttons for the form.
     */
    protected function add_standard_buttons() {
        $mform = $this->_form;
        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'save', get_string('saverule', 'local_bulkroleassign'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');
    }

    /**
     * Creates a rule and saves it based on the form data.
     */
    public function save() {
        $data = $this->get_data();
        $rule = new rule();
        $rule->title = $data->title;
        $rule->description = str_replace("\r\n", "\n", $data->description);
        $rule->role = $data->roleid;
        $rule->context = \context_coursecat::instance($data->categoryid);

        for ($i = 1; $i <= self::MAXFILTERS; $i++) {
            if (!empty($data->{"corefiltervalue$i"})) {
                $rule->add_filter($this->create_core_filter($rule, $i, $data));
            }
            if (!empty($data->{"customfiltervalue$i"})) {
                $rule->add_filter($this->create_custom_filter($rule, $i, $data));
            }
        }
        $rule->save();
        // Set a task that will run the rule.
        $task = new \local_bulkroleassign\task\run();
        $task->set_custom_data(array('ruleid' => $rule->id));
        \core\task\manager::queue_adhoc_task($task);
    }

    /**
     * Creates a core user profile filter.
     *
     * @param \local_bulkroleassign\local\rule $rule
     * @param int $number
     * @param array $data
     * @return \local_bulkroleassign\local\filter_user
     */
    protected function create_core_filter(rule $rule, $number, $data) {
        $prefix = 'corefilter';
        $field = $data->{"{$prefix}field{$number}"};
        $value = $data->{"{$prefix}value{$number}"};
        $method = $data->{"{$prefix}method{$number}"};
        return new filter_user($rule, $field, $value, $method);
    }

    /**
     * Creates a custom user profile rule.
     *
     * @param \local_bulkroleassign\local\rule $rule
     * @param type $number
     * @param type $data
     * @return \local_bulkroleassign\local\filter_user_info_data
     */
    protected function create_custom_filter(rule $rule, $number, $data) {
        $prefix = 'customfilter';
        $field = $data->{"{$prefix}field{$number}"};
        $value = $data->{"{$prefix}value{$number}"};
        $method = $data->{"{$prefix}method{$number}"};
        return new filter_user_info_data($rule, $field, $value, $method);
    }
}
