<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\output;

/**
 * Renderable for the index page
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2012 and later Nottingham University
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class index implements \renderable, \templatable {
    /** @var \single_button[] An array of buttons that do actions for the page. */
    public $controls;
    /** @var \local_bulkroleassign\local\rule[] an array of rules that should be displayed on the index page. */
    public $rules;

    /**
     * Constructor.
     *
     * @param \local_bulkroleassign\local\rule[] $rules An array of rules.
     */
    public function __construct($rules) {
        $addruleurl = new \moodle_url('/local/bulkroleassign/add.php');
        $addlabel = get_string('createrule', 'local_bulkroleassign');
        $this->controls = array(
            new \single_button($addruleurl, $addlabel),
        );
        $this->rules = $rules;
    }

    /**
     * @see \templatable::export_for_template
     * @param \renderer_base $output
     */
    public function export_for_template(\renderer_base $output) {
        $export = new \stdClass();
        // Generate the controls.
        $export->controls = array();
        foreach ($this->controls as $control) {
             $export->controls[] = $output->render($control);
        }
        // Strings common to all rules.
        $clone = get_string('clone', 'local_bulkroleassign');
        $deleterule = get_string('deleterule', 'local_bulkroleassign');
        $edit = get_string('editrule', 'local_bulkroleassign');
        $preview = get_string('preview', 'local_bulkroleassign');
        $run = get_string('run', 'local_bulkroleassign');
        // Parse the rules.
        $export->rules = array();
        foreach ($this->rules as $rule) {
            $exportrule = new \stdClass();
            $exportrule->id = $rule->id;
            $exportrule->title = $output->heading($rule->title, 3, 'title');
            $exportrule->description = $rule->description;
            $exportrule->context = $rule->context->get_context_name();
            $exportrule->role = $rule->get_role_name();
            $cloneurl = new \moodle_url('/local/bulkroleassign/clone.php', array('id' => $rule->id));
            $deleteusrl = new \moodle_url('/local/bulkroleassign/delete.php', array('id' => $rule->id));
            $editurl = new \moodle_url('/local/bulkroleassign/edit.php', array('id' => $rule->id));
            $previewurl = new \moodle_url('/local/bulkroleassign/preview.php', array('id' => $rule->id));
            $runurl = new \moodle_url('/local/bulkroleassign/run.php', array('id' => $rule->id));
            $exportrule->actions = array(
                $output->render(new \single_button($editurl, $edit, 'get')),
                $output->render(new \single_button($previewurl, $preview, 'get')),
                $output->render(new \single_button($cloneurl, $clone, 'get')),
                $output->render(new \single_button($deleteusrl, $deleterule, 'get')),
                $output->render(new \single_button($runurl, $run, 'get')),
            );
            $export->rules[] = $exportrule;
        }
        if (empty($export->rules)) {
            $export->norules = $output->notification(get_string('norules', 'local_bulkroleassign'), 'info');
        }
        return $export;
    }
}
