<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\output;

/**
 * The main render for the plugin
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class renderer extends \plugin_renderer_base {
    /**
     * Renders an index object.
     *
     * @param \local_bulkroleassign\output\index $index
     * @return string
     */
    public function render_index(index $index) {
        $data = $index->export_for_template($this);
        return parent::render_from_template('local_bulkroleassign/index', $data);
    }

    /**
     * Renders a preview object.
     *
     * @param \local_bulkroleassign\output\preview $preview
     * @return string
     */
    public function render_preview(preview $preview) {
        $data = $preview->export_for_template($this);
        return parent::render_from_template('local_bulkroleassign/preview', $data);
    }
}
