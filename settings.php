<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk Role Assignments Settings Page
 *
 * add a menu option to manage Bulk Role Assignments - Users->Permissions->Bulk Role Assign
 *
 * @package    local_bulkroleassign
 * @copyright  2012 and later Nottingham University
 * @author     Benjamin Ellis <benjamin.ellis@nottingham.ac.uk>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

if ($hassiteconfig) { // Needs this condition or there is error on login page.
    if (!$ADMIN->add('roles', new admin_externalpage('local_bulkroleassign',
            get_string('pluginname', 'local_bulkroleassign'),
            new moodle_url($CFG->wwwroot.'/local/bulkroleassign/index.php')))) {
        debugging('Failed to add menu Item', DEBUG_DEVELOPER);
    }
}
